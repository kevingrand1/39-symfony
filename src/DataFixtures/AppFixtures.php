<?php

namespace App\DataFixtures;

use Faker;
use DateTimeImmutable;
use App\Entity\Product;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 1; $i <= 10; $i ++) {
            $product = (new Product())
                // ->setName('Produit'.$i)
                // ->setSummary('Description du produit')
                // ->setPrice(rand(1, 500))
                // ->setIsActive(rand(0, 1))
                // ->setCreatedAt(new DateTimeImmutable())
                ->setName((string) $faker->words(4, true))
                ->setSummary($faker->sentence(10, true))
                ->setPrice($faker->randomFloat(2, 1, 500))
                ->setIsActive($faker->boolean(80))
                ->setCreatedAt(DateTimeImmutable::createFromMutable($faker->dateTimeThisMonth()))
            ;
            $manager->persist($product);

        }

        $manager->flush();
    }
}
