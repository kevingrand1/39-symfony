<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr' => [
                    // 'placeholder' => 'Paulo'
                    'placeholder' => 'contact.name.placeholder'
                ],
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    // 'placeholder' => 'paul@auchon.local'
                    'placeholder' => 'contact.email.placeholder'
                ],
                // 'help' => 'Aucun spam ne vous sera envoyé',
                'help' => 'contact.email.help',
                'constraints' => [
                    new NotBlank([
                        // 'message' => 'Veuillez renseigner votre adresse mail'
                        'message' => 'contact.email.blank',
                    ]),
                    new Email([
                        'message' => 'contact.email.invalid',
                    ]),
                ]
            ])
            ->add('subject',ChoiceType::class, [
                'choices' => [
                    'contact.subject.choices.one' => 1,
                    'contact.subject.choices.two' => 2,
                    'contact.subject.choices.three' => 3,
                ],
                'label' => 'contact.subject.label',
                'placeholder' => 'contact.subject.placeholder',
                'constraints' => [
                    new NotBlank([
                        'message' => 'contact.subject.blank',
                    ])
                ]

                ])
            ->add('message',TextareaType::class, [
                'label' => 'contact.message.label',
                'constraints' => [
                    new NotBlank([
                        'message' => 'contact.message.blank'
                    ]),
                    new Length([
                        'min' => 15,
                        'minMessage' => 'contact.message.length.min',
                    ]),
                ],
            ])
            ->add('gdpr', CheckboxType::class, [
                'label' => 'contact.gdpr.label',
                'constraints' => [
                    new NotBlank([
                        'message' => 'contact.gdpr.blank',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
