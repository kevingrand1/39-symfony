<?php

namespace App\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Symfony\Component\HttpFoundation\RequestStack;

class AppExtension extends AbstractExtension
{

    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('active', [$this, 'active']),
        ];
    }

    public function active(string $value): ?string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (str_contains($request->attributes->get('_route'), $value)){
            return 'active';
        } else {
            return null;
        }
    }
}
