<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="site_index")
     */
    public function index(): Response
    {
        return $this->render('site/index.html.twig');
    }

     /**
     * @Route("/a-propos", name="site_about")
     */
    public function about(): Response
    {
        return $this->render('site/about.html.twig');
    }

     /**
     * @Route("/contact", name="site_contact")
     */
    //HTTPfondation pour le Request
    public function contact(Request $request, TranslatorInterface $translator, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->get('name')->getData();
            $email = $form->get('email')->getData();
            $message = $form->get('message')->getData();

            //Recuperation en claire du sujet choisi dans le formulaire
            $subject = $form->get('subject');
            $choices = $subject->getConfig()->getOption('choices');
            $object = array_search($subject->getData(), $choices);
            $objectTranslated = $translator->trans($object);


            $email_templated = (new TemplatedEmail())
                ->from(new Address($email, $name))
                ->to('kevingrand123@gmail.com')
                ->subject($objectTranslated)
                ->textTemplate('mail/contact.txt.twig')
                ->context([
                    'name' => $name,
                    'mail' => $email,
                    'message' => $message,
                ])
            ;

            try {
                $mailer->send($email_templated);
                $this->addFlash('success', 'Votre message a bien été envoyé');
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('danger', 'Une erreur est survenue, veuillez réessayer plus tard');
            }

            return $this->redirectToRoute('site_contact');
        }
        
        return $this->render('site/contact.html.twig', [
            'contact_form' => $form->createView(),
        ]);
    }
}
